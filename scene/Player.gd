extends Area2D

export var speed = 400
export var accel = 800
export var friction = 2400
var screen_size
var v0 = Vector2.ZERO


func _ready():
	screen_size = get_viewport_rect().size
	position = screen_size / 2


func _process(dt):
	move3(dt)


# Move along a 2D surface where gravity is perpendicular to the surface, i.e., topdown motion.
# Directional input is the velocity vector with a maximum magnitude of `speed`.
# There is no acceleration;  starting and stopping are instantaneous.
#
# s = s0 + v * dt
func move1(dt):
	var input = get_input()
	var velocity = input * speed
	var new_position = position + velocity * dt

	position.x = clamp(new_position.x, 0, screen_size.x)
	position.y = clamp(new_position.y, 0, screen_size.y)
	animate(input)


# Move along a 2D surface where gravity is perpendicular to the surface, i.e., topdown motion.
# Directional input is the acceleration vector with a maximum magnitude of `accel`.
# Acceleration due to input is 0 once the magnitude of velocity reaches `speed`.
# If there is no input, but the object has velocity, then acceleration has magnitude `friction` and
# direction opposite to velocity.
# Acceleration due to friction is 0 once the velocity reaches 0.
#
# 	s = s0 + v0 * dt + a/2 * dt^2
func move2(dt):
	var input = get_input()
	var accel_eff = 0
	var vf = v0
	var displacement = Vector2.ZERO

	if input != Vector2.ZERO:
		accel_eff = accel * input
		displacement = v0 * dt + .5 * accel_eff * dt * dt
		vf = (v0 + accel_eff * dt).limit_length(speed)
	elif v0 != Vector2.ZERO:
		accel_eff = -friction * v0.normalized()
		displacement = v0 * dt + .5 * accel_eff * dt * dt
		vf = v0.move_toward(Vector2.ZERO, friction * dt)

	var new_position = position + displacement

	position.x = clamp(new_position.x, 0, screen_size.x)
	position.y = clamp(new_position.y, 0, screen_size.y)
	v0 = vf
	animate(input)


# Move along a 2D surface where gravity is perpendicular to the surface, i.e., topdown motion.
# Directional input is the acceleration vector.
# Input acceleration is resolved along velocity and perpendicular to velocity.
# Input acceleration component parallel to velocity is scaled  with a maximum magnitude of `accel`.
# Input acceleration component anti-parallel to velocity is scaled  with a maximum magnitude of
# `friction`.
# Acceleration due to input is 0 once the magnitude of velocity reaches `speed`.
# If there is no input, but the object has velocity, then acceleration has magnitude `friction` and
# direction opposite to velocity.
# Acceleration due to friction is 0 once the velocity reaches 0.
#
# 	s = s0 + v0 * dt + a/2 * dt^2
func move3(dt):
	var input = get_input()
	var accel_eff = Vector2.ZERO
	var vf = v0
	var displacement = Vector2.ZERO

	if input != Vector2.ZERO:
		if v0 != Vector2.ZERO:
			var input_parallel = input.project(v0)
			var accel_parallel_mag = accel if input_parallel.dot(v0) > 0 else friction
			var accel_parallel = accel_parallel_mag * input_parallel
			var input_normal = input.project(v0.tangent())
			var accel_normal = input_normal * accel
			accel_eff = accel_parallel + accel_normal
		else:
			accel_eff = accel * input
		displacement = v0 * dt + .5 * accel_eff * dt * dt
		vf = (v0 + accel_eff * dt).limit_length(speed)
	elif v0 != Vector2.ZERO:
		accel_eff = -friction * v0.normalized()
		displacement = v0 * dt + .5 * accel_eff * dt * dt
		vf = v0.move_toward(Vector2.ZERO, friction * dt)

	var new_position = position + displacement

	position.x = clamp(new_position.x, 0, screen_size.x)
	position.y = clamp(new_position.y, 0, screen_size.y)
	v0 = vf
	animate(input)


func move4(dt):
	# x = x0 + v0*t + .5*a*t^2
	# v_max = v0 + a*t --> t = (v_max - v0) / a
	# s = v0x + ax*t --> tx = (s - v0x) / ax
	# s = v0y + ay*t --> ty = (s - v0y) / ay
	# sqrt(v^2 + (at)^2)
	var input = get_input()
	var accel_eff = accel * input
	var dt_rem = max(0, speed - v0.length() / accel_eff)
	var displacement = v0 * dt + .5 * accel_eff * dt_rem * dt_rem
	var new_position = position + displacement

	position.x = clamp(new_position.x, 0, screen_size.x)
	position.y = clamp(new_position.y, 0, screen_size.y)
	v0 = (v0 + accel_eff * dt_rem)
	print(v0)
	animate(input)


func get_input():
	var input_x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	var input_y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	return Vector2(input_x, input_y).limit_length(1)
	

func animate(input):
	if input != Vector2.ZERO:
		if input.x != 0:
			$AnimatedSprite.animation = "horizontal"
			$AnimatedSprite.flip_h = input.x < 0
			$AnimatedSprite.flip_v = false
		elif input.y != 0:
			$AnimatedSprite.animation = "vertical"
			$AnimatedSprite.flip_v = input.y > 0
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.animation = "horizontal"
		$AnimatedSprite.flip_v = false
		$AnimatedSprite.stop()
